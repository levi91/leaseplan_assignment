package dtos;

import lombok.Data;

@Data
public class Product {

    private String image;

    private String unit;

    private String provider;

    private Object price;

    private String title;

    private String promoDetails;

    private String brand;

    private Boolean isPromo;

    private String url;

}