Feature: Search for products
  Returns searched product.

# Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
# Available products: "orange", "apple", "pasta", "cola"
# Prepare Positive and negative scenarios

  @Smoke
  Scenario Outline: Search product <productName>
   When Search for "<productName>" is called
   Then Status code returned is <statusCode>
   And Product "<productName>" is "<present>" in response
   Examples:
     | productName | statusCode | present |
     | apple       | 200        | present |
     | pasta       | 200        | present |
     | cola        | 200        | present |

  @Smoke
  Scenario: Search for orange and validate response is empty list
   When Search for "orange" is called
   Then Status code returned is 200
   And Response is empty list

  @Smoke
  Scenario Outline: Search for non-existing products
   When Search for "<productName>" is called
   Then Status code returned is <statusCode>
   Examples:
     | productName | statusCode |
     | banana      | 404        |
     | cucumber    | 404        |
     | pineapple   | 404        |
     | fanta       | 404        |