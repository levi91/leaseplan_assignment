package starter.stepdefinitions;

import dtos.Product;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import starter.api.SearchProductsAPI;

import java.util.List;

import static org.junit.Assert.*;

public class SearchStepDefinitions {

    private static final String PRESENT = "present";

    @Steps
    public SearchProductsAPI productsAPI;

    @When("Search for {string} is called")
    public void searchingProduct(final String productName) {
        Serenity.setSessionVariable("AllProducts").to(productsAPI.get(productName));
    }

    @And("Product {string} is {string} in response")
    public void checkProductInResponse(final String productName, final String present) {
        List<Product> allProducts = Serenity.sessionVariableCalled("AllProducts");

        if (PRESENT.equals(present)) {
            // Go through all the products from response and assert that at lest one title contains searched product
            // in case that we need to validate that in all products returned in response title contains the searched product
            // we would use softAssert to go through all the products
            assertTrue(allProducts.stream().anyMatch(product -> product.getTitle().toLowerCase().contains(productName)));
        } else {
            assertTrue(allProducts.stream().anyMatch(product -> !product.getTitle().toLowerCase().contains(productName)));
        }
    }

    @Then("Status code returned is {int}")
    public void checkStatusCodeReturned(final int statusCode) {
        assertEquals(statusCode, SerenityRest.lastResponse().getStatusCode());
    }

    @And("Response is empty list")
    public void validateEmptyResponse() {
        List<Product> allProducts = Serenity.sessionVariableCalled("AllProducts");
        assertNotNull(allProducts);
        assertTrue(allProducts.isEmpty());
    }

}