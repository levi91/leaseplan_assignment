package starter.api;

import dtos.Product;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.apache.hc.core5.http.HttpStatus;
import org.apache.http.entity.ContentType;

import java.util.Collections;
import java.util.List;


public class SearchProductsAPI {

    private static final String BASE_URL = "https://waarkoop-server.herokuapp.com/api/v1/search/demo";

    @Step("Get request")
    public List<Product> get(String product) {
        Response response = SerenityRest.given()
                .baseUri(BASE_URL)
                .log().all()
                .contentType(ContentType.APPLICATION_JSON.getMimeType())
                .when()
                .get(product)
                .andReturn();

        return response.getStatusCode() == HttpStatus.SC_NOT_FOUND ?
                Collections.emptyList() : response.getBody().jsonPath().getList("", Product.class);
    }

}